# SpeedCar

#### 介绍
[微信小游戏开发到上限速通手册]课程的源码，我们将会复刻一份线上版本的微信小游戏，微信扫描下方二维码可体验小游戏：
![输入图片说明](gh_3d9c25ef9f64_258.jpg)

#### 课程导读
分享的知识是如何使用Cocos Creator（3.5版本）迅速开发出一款小游戏(发布平台可以自己选择，例如微信小游戏、抖音小游戏)，教程中的小游戏是2D、单机类型的，请您加入时仔细甄别。

如星球名字所示，这个星球所分享的知识是如何迅速掌握开发小游戏的技能，所以这里可能不会有太多学术上的内容，并且如果不是必要时（例如性能优化、查找问题等）也不会对原理方面进行过多的阐述。本星球面向的想迅速掌握小游戏开发的开发者，如果您学习的目的不是想快速开发一款小游戏，而是其他目的，比如找工作、写博客等，请不要加入此星球，可能会对您产生“知其然而未知其所以然”的误导。



您和我都知道，如果想迅速掌握一门新的技术，首先就要对它的常见场景花上一些琢磨的时间，琢磨到大概的程度可就可以开始开工了，细节方面在开发过程中只要有一本可以随时查阅的手册，基本上就没问题了。本星球的教程将会直接把我琢磨出来的结论告诉您，让您节省大把的时间和脑细胞。本教程目前将会以一款已经发布到微信平台的赛车小游戏的开发过程为例子，一步步展开式的教会您如何从0到1开发出一款可以发布到微信平台的小游戏。



现在，请在微信小游戏中搜索“拉力赛车漂移竞速赛”，这个就是我们本教程中将要实现的游戏。如您所见，我们需要实现游戏的素材制作、地图制作、专注表演的NPC，还有重要的UI界面，以及最重要的广告播放逻辑。也如同您可能一眼看不出来的，我们需要实现小车的运动逻辑，为了让小车的运动有迫真的效果，我们不能如同那些不负责任并且也不知被复制了几遍的博客里所说的那样直接粗暴的每秒设置小车的位置，而是引入Cocos Creator擅长的物理系统。



在下面的章节中我会将整体实现拆解成一个一个的步骤，我保证只要您按照接下来的步骤一步步去实现，您一定能在很短的时间内迅速开发出一款人类高质量小游戏。如果您的技能树里点的比较宽，比如会后端，那么只要bing一下如何在Cocos中调用http，您将会创造出更多的可能，例如有社交属性的小游戏或是当前比较火的直播狗子蹦迪的直播软件。总之，通过学习后，您一定能至少掌握如何开发一款2D类型的单机小游戏，如果您最终没有达到以上目标的话，我将会相信昨天让我v他五十块的一定是始皇赵政。


#### 如何使用此示例文档

1.  先在您的Cocos Dashboard中新建一个项目：

![输入图片说明](WechatIMG509.png)

2.  用VS Code打开项目所在文件及：

![输入图片说明](WechatIMG510.png)

3.  打开项目后进入到git管理，先提交下引擎的代码：

![输入图片说明](WechatIMG516.png)

4.  添加远程仓库：

![输入图片说明](WechatIMG512.png)

5.  填入本仓库地址：

![输入图片说明](WechatIMG513.png)

6.  写个远程仓库的名字：

![输入图片说明](WechatIMG514.png)

7.  点击屏幕左下方的分支管理后您就可以切换到指定课程章节对应的tag了：

![输入图片说明](WechatIMG517.png)

#### 知识星球

课程已经放到知识星球啦，以后不用QQ/VX群的方式了。点击以下链接就可以加入了：

https://t.zsxq.com/0f4SBLCHN
