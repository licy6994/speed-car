import { _decorator, Component, Node, input, Input, Vec2, Vec3, Camera, misc, UITransform } from 'cc';
const { ccclass, property } = _decorator;

@ccclass('TouchInput')
export class TouchInput {

    private static singleiInstance: TouchInput = new TouchInput();

    touchX: number = 0;

    touchY: number = 0;

    inTouch: boolean = false

    static Instance() {
        return TouchInput.singleiInstance;
    };

    private constructor() {     
        input.on(Input.EventType.TOUCH_MOVE, (event)=>{
            this.inTouch = true
            var pos = new Vec2(event.getLocation())
            this.touchX = pos.x
            this.touchY = pos.y
            console.log('移动中 点击的X坐标： '+this.touchX+  ' 点击的Y坐标：'+this.touchY + ' 点击状态：'+this.inTouch)
        })
        input.on(Input.EventType.TOUCH_START,(event)=>{
            this.inTouch = true
            var pos = new Vec2(event.getLocation())
            this.touchX = pos.x
            this.touchY = pos.y
            console.log('开始点击 点击的X坐标： '+this.touchX+  ' 点击的Y坐标：'+this.touchY+ ' 点击状态：'+this.inTouch)
        })
        input.on(Input.EventType.TOUCH_CANCEL,(event)=>{
            this.inTouch = false
            console.log('点击结束 点击的X坐标： '+this.touchX+  ' 点击的Y坐标：'+this.touchY+ ' 点击状态：'+this.inTouch)
        })
        input.on(Input.EventType.TOUCH_END,(event)=>{
            this.inTouch = false
            console.log('点击结束 点击的X坐标： '+this.touchX+  ' 点击的Y坐标：'+this.touchY+ ' 点击状态：'+this.inTouch)
        })
    }

    /**
     * 判断是当前触控点是在node节点的左边还是在右边
     * @param node 参照的节点，也就是汽车节点
     * @param camera 相机组件 用来转换触控点坐标为世界系下的坐标
     * @returns 左边返回-1 右边返回1 保持直行则返回0
     */
    public getHorizontal(node:Node, camera: Camera){
        if(this.inTouch==false){
            return 0;
        }
        //触控点的世界左边
        var twPos =  new Vec3(camera.screenToWorld(new Vec3(this.touchX,this.touchY)))
        //参照的节点也就是汽车节点的世界坐标
        var nwPos = new Vec3(node.getWorldPosition())
        //计算以汽车节点为原点的触控点的相对坐标
        var relativePos = new Vec3((twPos.x-nwPos.x),(twPos.y-nwPos.y))
        //目前汽车已经旋转的角度  使用前先转换一下
        var nodeRad = misc.degreesToRadians(node.angle)
        //旋转nodeRad度的逻辑
        relativePos = new Vec3((relativePos.x * Math.cos(nodeRad) + relativePos.y * Math.sin(nodeRad)),
            (-relativePos.x * Math.sin(nodeRad)+relativePos.y * Math.cos(nodeRad)));


        //如果触控点在车的垂直宽度内，则视为直行
        if(Math.abs(relativePos.x)<node.getComponent(UITransform).width/2){
            return 0;
        }
        if(relativePos.x==0){
            return 0;
        }
        if(relativePos.x>0){
            return 1;
        }
        return -1;        
    }

    /**
     * 判断是当前触控点是在node节点的左边还是在右边
     * @param node 参照的节点，也就是汽车节点
     * @param camera 相机组件 用来转换触控点坐标为世界系下的坐标
     * @returns 向后返回-1 向前返回1 松开油门则返回0
     */
    public getVertical(node:Node, camera: Camera){
        if(this.inTouch==false){
            return 0;
        }
        //触控点的世界左边
        var twPos =  new Vec3(camera.screenToWorld(new Vec3(this.touchX,this.touchY)))
        //参照的节点也就是汽车节点的世界坐标
        var nwPos = new Vec3(node.getWorldPosition())
        //计算以汽车节点为原点的触控点的相对坐标
        var relativePos = new Vec3((twPos.x-nwPos.x),(twPos.y-nwPos.y))
        //目前汽车已经旋转的角度  使用前先转换一下
        var nodeRad = misc.degreesToRadians(node.angle)
        //旋转nodeRad度的逻辑
        relativePos = new Vec3((relativePos.x * Math.cos(nodeRad) + relativePos.y * Math.sin(nodeRad)),
            (-relativePos.x * Math.sin(nodeRad)+relativePos.y * Math.cos(nodeRad)));


        //如果触控点在车的垂直高度内，则视为不前进也不后退
        if(Math.abs(relativePos.y)<node.getComponent(UITransform).height/2){
            return 0;
        }
        if(relativePos.y==0){
            return 0;
        }
        if(relativePos.y>0){
            return 1;
        }
        return -1;        
    }
}

